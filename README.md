# Minimum Cost over a 2D grid for NEO

To solve the planning problem the following assumptions were made:
* Since the robot model was not given, and it was mentioned that robot is quite maneuverable I assume that it is an omnidirectional robot, i.e., it can move in both x and y-axis without needing to ever turn about its axis.
* It is assumed that the robot always stays exactly at the center of each box. This means that the time to move one block would be 2 sec.
* It is assumed that the starting position of the robot is (0,0) which is located at the top left corner of the grid. The location of each cell will be referred to as a pair (row, col). For example, the bottom right cell with be (9,9).
* The user is requested to input waypoints in terms of row and column position of the cell. For example, the (row, col) value of the top-right cell is (0,9), i.e., 0th row and 9th column.

For the non-bonus part, the optimal cost between any two given waypoints can be easily calculated in closed form using the Manhattan metric, i.e.,
```math
\mathcal{M}((x_1,y_1),(x_2,y_2)) = |x_1 - x_2 | + |y_1 - y_2|
```
because the weights of every grid cell is the same. Since this is a closed-form expression, in terms of implementation it would be more optimal compared to other iterative algorithms. However, to make things interesting these distances are computed using the standard Dijkstra’s algorithm.


Each waypoint is expressed as a node $`\mathcal{N}`$ and time to reach from one waypoint to all of the following waypoints is expressed as an edge $`\mathcal{E}`$. The problem is then converted into a weighted directed graph $`\mathcal{G}(\mathcal{N},\mathcal{E})`$ and is solved using Dijkstra’s algorithm for weighted graphs. Of course, Dijkstra’s is not the most optimal algorithm computing costs over digraphs, however reducing computation cost and complexity of algorithms is beyond the scope of this task. To make things interesting I have provided an STL implementation of Dijkstra's algorithm.

## How to run the code
It is very simple. All we need is to run the main.cpp file. One can gitclone the code file or simply copy/paste code in his/her fav txt editor or VIM. To gitclone, run the following command in terminal.
```
git clone git@gitlab.com:a5akhtar/minimumcost.git
```
To run the code navigate to the folder where the cpp file is, and run the following command in the terminal
```
g++ -o output main.cpp
```
This will create an executable in the same folder. To run the executable simply type the following command
```
./output
```
An output similar to this is expected
```
To run the robot in Hazardous arena press '1'
or press any key followed by 'enter' to run the program normally 
2
Enter number of way points. It should be an integer between 1 to 100: 1
Enter row number of waypoint #1: 0
Enter column number of waypoint #1: 9
Enter penalty of waypoint #1: 9999
Vertex Distance from Source
0 		 0
1 		 38
2 		 56


 The lowest cost for the mission is: 56

```

