// C++ program to get least cost path in a grid from 
// top-left to bottom-right 
#include <bits/stdc++.h> 
using namespace std; 
# define INF 0x3f3f3f3f 

#define ROW 10 
#define COL 10 

// structure for information of each cell 
struct cell 
{ 
	int x, y; 
	int distance; 
	cell(int x, int y, int distance) : 
		x(x), y(y), distance(distance) {} 
}; 

// This class represents a directed graph using 
// adjacency list representation 
class Graph 
{ 
	int V; // No. of vertices 

	// In a weighted graph, we need to store vertex 
	// and weight pair for every edge 
	list< pair<int, int> > *adj; 

public: 
	Graph(int V); // Constructor 

	// function to add an edge to graph 
	void addEdge(int u, int v, int w); 

	// prints shortest path from s 
	void shortestPath(int s); 
}; 

// Allocates memory for adjacency list 
Graph::Graph(int V) 
{ 
	this->V = V; 
	adj = new list< pair<int, int> >[V]; 
} 

void Graph::addEdge(int u, int v, int w) 
{ 
	adj[u].push_back(make_pair(v, w)); 
	//adj[v].push_back(make_pair(u, w)); 
} 

// Prints shortest paths from src to all other vertices 
void Graph::shortestPath(int src) 
{ 
	// Create a set to store vertices that are being 
	// prerocessed 
	set< pair<int, int> > setds; 

	// Create a vector for distances and initialize all 
	// distances as infinite (INF) 
	vector<int> dist(V, INF); 

	// Insert source itself in Set and initialize its 
	// distance as 0. 
	setds.insert(make_pair(0, src)); 
	dist[src] = 0; 

	/* Looping till all shortest distance are finalized 
	then setds will become empty */
	while (!setds.empty()) 
	{ 
		// The first vertex in Set is the minimum distance 
		// vertex, extract it from set. 
		pair<int, int> tmp = *(setds.begin()); 
		setds.erase(setds.begin()); 

		// vertex label is stored in second of pair (it 
		// has to be done this way to keep the vertices 
		// sorted distance (distance must be first item 
		// in pair) 
		int u = tmp.second; 

		// 'i' is used to get all adjacent vertices of a vertex 
		list< pair<int, int> >::iterator i; 
		for (i = adj[u].begin(); i != adj[u].end(); ++i) 
		{ 
			// Get vertex label and weight of current adjacent 
			// of u. 
			int v = (*i).first; 
			int weight = (*i).second; 

			// If there is shorter path to v through u. 
			if (dist[v] > dist[u] + weight) 
			{ 
				/* If distance of v is not INF then it must be in 
					our set, so removing it and inserting again 
					with updated less distance. 
					Note : We extract only those vertices from Set 
					for which distance is finalized. So for them, 
					we would never reach here. */
				if (dist[v] != INF) 
					setds.erase(setds.find(make_pair(dist[v], v))); 

				// Updating distance of v 
				dist[v] = dist[u] + weight; 
				setds.insert(make_pair(dist[v], v)); 
			} 
		} 
	} 

	// Print shortest distances stored in dist[] 
	printf("Vertex Distance from Source\n"); 
	for (int i = 0; i < V; ++i) 
		printf("%d \t\t %d\n", i, dist[i]); 
	printf("\n\n The lowest cost for the mission is: %d\n\n", dist[V-1]);
} 

// Utility method for comparing two cells 
bool operator<(const cell& a, const cell& b) 
{ 
	if (a.distance == b.distance) 
	{ 
		if (a.x != b.x) 
			return (a.x < b.x); 
		else
			return (a.y < b.y); 
	} 
	return (a.distance < b.distance); 
} 

// Utility method to check whether a point is 
// inside the grid or not 
bool isInsideGrid(int i, int j) 
{ 
	return (i >= 0 && i < COL && j >= 0 && j < ROW); 
} 

// Method returns minimum cost to reach bottom 
// right from top left 
int shortest(int grid[ROW][COL], int row, int col, int s_row, int s_col, int f_row, int f_col) 
{ 
	int dis[row][col]; 

	// initializing distance array by INT_MAX 
	for (int i = 0; i < row; i++) 
		for (int j = 0; j < col; j++) 
			dis[i][j] = INT_MAX; 

	// direction arrays for simplification of getting 
	// neighbour 
	int dx[] = {-1, 0, 1, 0}; 
	int dy[] = {0, 1, 0, -1}; 

	set<cell> st; 

	// insert the given cell with 0 distance 
	st.insert(cell(s_row, s_col, 0)); 
	    
    // initialize distance of (0, 0) with its initial grid value 
	dis[s_row][s_col] = grid[s_row][s_col]; 

	// loop for standard dijkstra's algorithm 
	while (!st.empty()) 
	{ 
		// get the cell with minimum distance and delete 
		// it from the set 
		cell k = *st.begin(); 
		st.erase(st.begin()); 

		// looping through all neighbours 
		for (int i = 0; i < 4; i++) 
		{ 
			int x = k.x + dx[i]; 
			int y = k.y + dy[i]; 

			// if not inside boundary, ignore them 
			if (!isInsideGrid(x, y)) 
				continue; 

			// If distance from current cell is smaller, then 
			// update distance of neighbour cell 
			if (dis[x][y] > dis[k.x][k.y] + grid[x][y]) 
			{ 
				// If cell is already there in set, then 
				// remove its previous entry 
				if (dis[x][y] != INT_MAX) 
					st.erase(st.find(cell(x, y, dis[x][y]))); 

				// update the distance and insert new updated 
				// cell in set 
				dis[x][y] = dis[k.x][k.y] + grid[x][y]; 
				st.insert(cell(x, y, dis[x][y])); 
			} 
		} 
	} 

	int desired_cell = dis[f_row][f_col]; // shortest distance to the desired cell
	return desired_cell;
	//return dis[row - 1][col - 1]; // shortest distance to the final cell
} 


// Main loop
int main() 
{ 
	int total_cost = 0;
	int cost_to_go = 0;
	int cleaning_cost = 20;
	int cost_to_go_and_clean = 0;
	int wp_a = 0;
	int wp_b = 0;
	int missed_waypoints = 0;
	int penality_for_missing = 0;
	int edge_weight = 0;
	int mode = 0;
	int num = 1; 
	int penality = 0;
	int r_waypoint = 0;
	int c_waypoint = 0;
	int number_of_waypoints = 0;

	// setting up a 2D grid initialized with zeros
	int grid[ROW][COL] = {0};
	// assigning the cost of each cell with 2
	for (int i = 0; i < 10; i ++)
		{
			for (int j = 0; j < 10; j ++)
			{
				grid[i][j] = 2;
			}
		}

	// Select arena for the robot motion
	cout << "To run the robot in Hazardous arena press '1'" << endl, 
	cout << "or press any key followed by 'enter' to run the program normally " << endl; 
    cin >> mode; 
	// assigning the cost of hazardous cells with 5
	if (mode == 1)
	{
		for (int i = 3; i < 7; i ++)
		{
			for (int j = 3; j < 7; j ++)
			{
				grid[i][j] = 5;
			}
		}
	}	
	
	// Input way points and penalty values
	// Declare the variables 
    
    // Input the integer 
    cout << "Enter number of way points. It should be an integer between 1 to 100: "; 
    cin >> num; 
	
	while (num <= 0 || num >100)
	{
		cout << "number of out of range!" << endl;
		cout << "Please renter number of way points: ";
		cin >> num;
	}
	
	// Display the integer 
    //cout << "Entered integer is: " << num << endl; 

	int way_points[num + 2][3] = {0};
	for (int i =1; i<num+1; i++)
	{
		cout << "Enter row number of waypoint #" << i <<": " ;
		cin >> r_waypoint;
		while (r_waypoint < 0 || r_waypoint > 9)
		{
			cout << "number of out of range!" << endl;
			cout << "Please renter row number between 0 and 9: ";
			cin >> r_waypoint;
		}
		cout << "Enter column number of waypoint #" << i <<": " ;
		cin >> c_waypoint;
		while (c_waypoint < 0 || c_waypoint > 9)
		{
			cout << "number of out of range!" << endl;
			cout << "Please renter column number between 0 and 9: ";
			cin >> c_waypoint;
		}
		cout << "Enter penalty of waypoint #" << i <<": " ;
		cin >> penality;
		while (penality < 0 )
		{
			cout << "penality can never be negative!" << endl;
			cout << "Please renter a valid penality number: ";
			cin >> penality;
		}
	way_points[i][0] = penality;
	way_points[i][1] = r_waypoint;
	way_points[i][2] = c_waypoint;
	}	
	way_points[num+1][0] = INT_MAX; 
	way_points[num+1][1] = 9; //Final position
	way_points[num+1][2] = 9; //Final position
	    
		
	number_of_waypoints = num+2; 
	/*int way_points [number_of_waypoints][3] = 
	{
		0, 0, 0,
		4, 0, 9,
		//1, 0, 9,
		//1, 4, 9,
		0, 9, 9
	};*/
	Graph g(number_of_waypoints);

	for (int i = 0; i < number_of_waypoints-1; i++) 
	{
		for (int j = i+1; j<number_of_waypoints; j++)
		{
			wp_a = i;
	    	wp_b = j;
			cost_to_go =  shortest(grid, ROW, COL, way_points[wp_a][1],way_points[wp_a][2],way_points[wp_b][1],way_points[wp_b][2]);
			cost_to_go_and_clean = cost_to_go + cleaning_cost - grid[way_points[wp_a][1]][way_points[wp_b][2]];
			if (j == number_of_waypoints -1)
			{
				// We assume that simply go to the docking position and don't clean it, unless specified as a seperate way point
				cost_to_go_and_clean = cost_to_go_and_clean - cleaning_cost;
			}
			// calculation number of missed waypoints and adding penalities
			missed_waypoints = (j - i) -1;
			for (int k = 0; k < missed_waypoints; k++)
			{
				penality_for_missing = penality_for_missing + way_points[i + k + 1][0];
			}
			edge_weight = penality_for_missing + cost_to_go_and_clean;
			// making a weighted edge from wp_a to wp_b
			g.addEdge(wp_a, wp_b, edge_weight);
			penality_for_missing = 0;
		}		
	}
	// Calculating lowest cost over a weighted digraph usjing Dijkstra’s shortest path algorithm
	g.shortestPath(0); 

	return 0; 
} 

